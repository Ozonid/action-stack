import React from 'react';
import ActionStackProvider, { ActionStackProviderProps } from '../context/ActionStackProvider';

const withActionStack = (
  WrappedComponent: React.ComponentType,
  { onUpdate, initialValue }: ActionStackProviderProps,
) => () => (
  <ActionStackProvider initialValue={initialValue} onUpdate={onUpdate}>
    <WrappedComponent />
  </ActionStackProvider>
);

export default withActionStack;
