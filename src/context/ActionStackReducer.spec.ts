import { ActionStackProviderState } from './ActionStackProvider';
import { ActionStackActionTypes, ActionStackReducer } from './ActionStackReducer';

describe('ActionStackReducer', () => {
  describe('ON_ACTION', () => {
    it('updates data, pushed action into stack and updates position', () => {
      const stack = [
        {
          change: 1,
        },
      ];
      const initialState: ActionStackProviderState = {
        data: {
          test: 'test',
          foo: 'bar',
        },
        reference: {
          test: 'test',
          foo: 'bar',
        },
        position: 1,
        stack,
        onUpdate: jest.fn(),
      };
      const payload = {
        bip: 'bop',
        foo: 'rab',
      };

      const result = ActionStackReducer(initialState, { type: ActionStackActionTypes.ON_ACTION, payload });

      expect(result.data).toEqual({
        test: 'test',
        bip: 'bop',
        foo: 'rab',
      });
      expect(result.reference).toEqual({
        test: 'test',
        foo: 'bar',
      });
      expect(result.position).toEqual(2);
      expect(result.stack).toEqual([...stack, payload]);
      expect(initialState.onUpdate).not.toHaveBeenCalled();
    });

    it('updates reference if there are already 20 elements in the stack and removes the first element', () => {
      const stack = new Array(20).fill({
        bip: 'bop',
      });
      const initialState: ActionStackProviderState = {
        data: {
          test: 'test',
          foo: 'bar',
          bip: 'bop',
        },
        reference: {
          test: 'test',
          foo: 'bar',
        },
        position: 20,
        stack,
        onUpdate: jest.fn(),
      };
      const payload = {
        foo: 'rab',
      };

      const result = ActionStackReducer(initialState, { type: ActionStackActionTypes.ON_ACTION, payload });

      expect(result.data).toEqual({
        test: 'test',
        bip: 'bop',
        foo: 'rab',
      });
      expect(result.reference).toEqual({
        test: 'test',
        foo: 'bar',
        bip: 'bop',
      });
      expect(result.position).toEqual(20);
      expect(result.stack).toEqual([...stack.slice(1, 20), payload]);
      expect(initialState.onUpdate).not.toHaveBeenCalled();
    });

    it('cuts off the stack if we are in a previous position and a new action arrives', () => {
      const stack = new Array(10).fill({
        bip: 'bop',
      });
      const initialState: ActionStackProviderState = {
        data: {
          test: 'test',
          foo: 'bar',
          bip: 'bop',
        },
        reference: {
          test: 'test',
          foo: 'bar',
        },
        position: 5,
        stack,
        onUpdate: jest.fn(),
      };
      const payload = {
        foo: 'rab',
      };

      const result = ActionStackReducer(initialState, { type: ActionStackActionTypes.ON_ACTION, payload });

      expect(result.data).toEqual({
        test: 'test',
        bip: 'bop',
        foo: 'rab',
      });
      expect(result.reference).toEqual({
        test: 'test',
        foo: 'bar',
      });
      expect(result.position).toEqual(6);
      expect(result.stack).toEqual([...stack.slice(0, 5), payload]);
      expect(initialState.onUpdate).not.toHaveBeenCalled();
    });
  });

  describe('ON_UNDO', () => {
    it('rolls back the last change', () => {
      const stack = new Array(2).fill(0).map((v, idx) => ({ change: idx + 1 }));
      const initialState: ActionStackProviderState = {
        data: {
          test: 'test',
          foo: 'bar',
          change: 2,
        },
        reference: {
          test: 'test',
          foo: 'bar',
        },
        position: 2,
        stack,
        onUpdate: jest.fn(),
      };

      const result = ActionStackReducer(initialState, { type: ActionStackActionTypes.ON_UNDO, payload: 1 });

      expect(result.data).toEqual({
        test: 'test',
        foo: 'bar',
        change: 1,
      });
      expect(result.reference).toEqual({
        test: 'test',
        foo: 'bar',
      });
      expect(result.position).toEqual(1);
      expect(result.stack).toEqual(stack);
      expect(initialState.onUpdate).toHaveBeenCalledWith({
        test: 'test',
        foo: 'bar',
        change: 1,
      });
    });

    it('rolls back last 3 changes', () => {
      const stack = new Array(10).fill(0).map((v, idx) => ({ change: idx + 1 }));
      const initialState: ActionStackProviderState = {
        data: {
          test: 'test',
          foo: 'bar',
          change: 10,
        },
        reference: {
          test: 'test',
          foo: 'bar',
        },
        position: 5,
        stack,
        onUpdate: jest.fn(),
      };

      const result = ActionStackReducer(initialState, { type: ActionStackActionTypes.ON_UNDO, payload: 3 });

      expect(result.data).toEqual({
        test: 'test',
        foo: 'bar',
        change: 2,
      });
      expect(result.reference).toEqual({
        test: 'test',
        foo: 'bar',
      });
      expect(result.position).toEqual(2);
      expect(result.stack).toEqual(stack);
      expect(initialState.onUpdate).toHaveBeenCalledWith({
        test: 'test',
        foo: 'bar',
        change: 2,
      });
    });

    it('prevents position going below 0', () => {
      const stack = [
        {
          change: 1,
        },
        {
          change: 2,
        },
      ];
      const initialState: ActionStackProviderState = {
        data: {
          test: 'test',
          foo: 'bar',
          change: 2,
        },
        reference: {
          test: 'test',
          foo: 'bar',
        },
        position: 2,
        stack,
        onUpdate: jest.fn(),
      };

      const result = ActionStackReducer(initialState, { type: ActionStackActionTypes.ON_UNDO, payload: 10 });

      expect(result.data).toEqual({
        test: 'test',
        foo: 'bar',
      });
      expect(result.reference).toEqual({
        test: 'test',
        foo: 'bar',
      });
      expect(result.position).toEqual(0);
      expect(result.stack).toEqual(stack);
      expect(initialState.onUpdate).toHaveBeenCalledWith({
        test: 'test',
        foo: 'bar',
      });
    });
  });

  describe('ON_REDO', () => {
    it('redoes the last change', () => {
      const stack = new Array(3).fill(0).map((v, idx) => ({ change: idx + 1 }));
      const initialState: ActionStackProviderState = {
        data: {
          test: 'test',
          foo: 'bar',
          change: 1,
        },
        reference: {
          test: 'test',
          foo: 'bar',
        },
        position: 1,
        stack,
        onUpdate: jest.fn(),
      };

      const result = ActionStackReducer(initialState, { type: ActionStackActionTypes.ON_REDO, payload: 1 });

      expect(result.data).toEqual({
        test: 'test',
        foo: 'bar',
        change: 2,
      });
      expect(result.reference).toEqual({
        test: 'test',
        foo: 'bar',
      });
      expect(result.position).toEqual(2);
      expect(result.stack).toEqual(stack);
      expect(initialState.onUpdate).toHaveBeenCalledWith({
        test: 'test',
        foo: 'bar',
        change: 2,
      });
    });

    it('redoes last 3 changes', () => {
      const stack = new Array(10).fill(0).map((v, idx) => ({ change: idx + 1 }));
      const initialState: ActionStackProviderState = {
        data: {
          test: 'test',
          foo: 'bar',
          change: 1,
        },
        reference: {
          test: 'test',
          foo: 'bar',
        },
        position: 1,
        stack,
        onUpdate: jest.fn(),
      };

      const result = ActionStackReducer(initialState, { type: ActionStackActionTypes.ON_REDO, payload: 3 });

      expect(result.data).toEqual({
        test: 'test',
        foo: 'bar',
        change: 4,
      });
      expect(result.reference).toEqual({
        test: 'test',
        foo: 'bar',
      });
      expect(result.position).toEqual(4);
      expect(result.stack).toEqual(stack);
      expect(initialState.onUpdate).toHaveBeenCalledWith({
        test: 'test',
        foo: 'bar',
        change: 4,
      });
    });

    it('prevents position going above stack length', () => {
      const stack = new Array(10).fill(0).map((v, idx) => ({ change: idx + 1 }));
      const initialState: ActionStackProviderState = {
        data: {
          test: 'test',
          foo: 'bar',
          change: 1,
        },
        reference: {
          test: 'test',
          foo: 'bar',
        },
        position: 1,
        stack,
        onUpdate: jest.fn(),
      };

      const result = ActionStackReducer(initialState, { type: ActionStackActionTypes.ON_REDO, payload: 300 });

      expect(result.data).toEqual({
        test: 'test',
        foo: 'bar',
        change: 10,
      });
      expect(result.reference).toEqual({
        test: 'test',
        foo: 'bar',
      });
      expect(result.position).toEqual(10);
      expect(result.stack).toEqual(stack);
      expect(initialState.onUpdate).toHaveBeenCalledWith({
        test: 'test',
        foo: 'bar',
        change: 10,
      });
    });
  });
});
