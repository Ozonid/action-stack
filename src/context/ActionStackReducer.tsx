import { UnknownObject } from '../models/UnknownObject';
import { ActionStackProviderState } from './ActionStackProvider';

export enum ActionStackActionTypes {
  ON_ACTION = 'ON_ACTION',
  ON_UNDO = 'ON_UNDO',
  ON_REDO = 'ON_REDO',
}

interface OnAction {
  type: ActionStackActionTypes.ON_ACTION;
  payload: UnknownObject;
}

interface OnUndo {
  type: ActionStackActionTypes.ON_UNDO;
  payload: number;
}

interface OnRedo {
  type: ActionStackActionTypes.ON_REDO;
  payload: number;
}

type ActionStackActions = OnAction | OnUndo | OnRedo;

const onAction = (state: ActionStackProviderState, action: UnknownObject) => {
  let stack = state.stack;
  if (state.position !== state.stack.length) {
    stack = state.stack.slice(0, state.position);
  }

  let ref = { ...state.reference };
  if (stack.length === 20) {
    const mutation = stack[0];
    stack = stack.slice(1, 20);

    ref = {
      ...state.reference,
      ...mutation,
    };
  }

  return {
    ...state,
    data: {
      ...state.data,
      ...action,
    },
    stack: [...stack, action],
    position: stack.length + 1,
    reference: { ...ref },
  };
};

const navigateHistory = (state: ActionStackProviderState, pos: number) => {
  const newState = {
    ...state,
    position: pos,
    data: state.stack.slice(0, pos).reduce((result, step) => {
      return {
        ...result,
        ...step,
      };
    }, state.reference),
  };

  state.onUpdate(newState.data);
  return newState;
};

const onUndo = (state: ActionStackProviderState, step: number) => {
  const pos = Math.max(state.position - step, 0);
  return navigateHistory(state, pos);
};

const onRedo = (state: ActionStackProviderState, step: number) => {
  const pos = Math.min(state.position + step, state.stack.length);
  return navigateHistory(state, pos);
};

export const ActionStackReducer = (state: ActionStackProviderState, action: ActionStackActions): any => {
  switch (action.type) {
    case ActionStackActionTypes.ON_ACTION:
      return onAction(state, action.payload);
    case ActionStackActionTypes.ON_UNDO:
      return onUndo(state, action.payload);
    case ActionStackActionTypes.ON_REDO:
      return onRedo(state, action.payload);
    default:
      throw new Error('unknown action');
  }
};
