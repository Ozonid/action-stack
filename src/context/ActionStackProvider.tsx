import React, { createContext, useReducer } from 'react';
import { ActionStackReducer } from './ActionStackReducer';
import { UnknownObject } from '../models/UnknownObject';

export interface ActionStackProviderProps {
  initialValue?: UnknownObject;
  onUpdate: (value: UnknownObject) => void;
}

export const ActionStackContext = createContext<any>(undefined);
export interface ActionStackProviderState {
  data: UnknownObject;
  reference: UnknownObject;
  stack: UnknownObject[];
  position: number;
  onUpdate: (value: UnknownObject) => void;
}

const ActionStackProvider: React.FC<ActionStackProviderProps> = ({ initialValue, onUpdate, children }) => {
  const reducer = useReducer(ActionStackReducer, {
    data: initialValue,
    reference: initialValue,
    stack: [],
    position: 0,
    onUpdate,
  });
  return <ActionStackContext.Provider value={reducer}>{children}</ActionStackContext.Provider>;
};

export default ActionStackProvider;
