export interface UnknownObject {
  [s: string]: unknown;
}
