import { useContext } from 'react';
import { ActionStackContext } from '../context/ActionStackProvider';
import { ActionStackActionTypes } from '../context/ActionStackReducer';
import { UnknownObject } from '../models/UnknownObject';

interface UseActionStack {
  data: UnknownObject;
  isUndoAvailable: boolean;
  isRedoAvailable: boolean;
  stackLength: number;
  onAction: (value: UnknownObject) => void;
  onUndo: (step?: number) => void;
  onRedo: (step?: number) => void;
}

const useActionStack: () => UseActionStack = () => {
  const [state, dispatch] = useContext(ActionStackContext);

  const onAction = (data: UnknownObject) => {
    dispatch({
      type: ActionStackActionTypes.ON_ACTION,
      payload: data,
    });
  };

  const onUndo = (step: number = 1) => {
    dispatch({
      type: ActionStackActionTypes.ON_UNDO,
      payload: step,
    });
  };

  const onRedo = (step: number = 1) => {
    dispatch({
      type: ActionStackActionTypes.ON_REDO,
      payload: step,
    });
  };

  return {
    data: state.data,
    isUndoAvailable: state.position > 0,
    isRedoAvailable: state.position < state.stack.length,
    stackLength: state.stack.length,
    onAction,
    onUndo,
    onRedo,
  };
};

export default useActionStack;
