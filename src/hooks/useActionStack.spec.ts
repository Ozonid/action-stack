import React from 'react';
import useActionStack from './useActionStack';
import { ActionStackActionTypes } from '../context/ActionStackReducer';

describe('useActionStack', () => {
  let state;
  let dispatch;

  beforeEach(() => {
    state = {
      stack: new Array(10),
      position: 2,
      data: {
        foo: 'bar',
      },
    };
    dispatch = jest.fn();

    const mockUseContext = jest.fn().mockImplementation(() => [state, dispatch]);
    React.useContext = mockUseContext;
  });

  it('isUndoAvailable, isRedoAvailable, stackLength and data are properly available', async () => {
    const { isUndoAvailable, isRedoAvailable, stackLength, data } = useActionStack();

    expect(isUndoAvailable).toBeTruthy();
    expect(isRedoAvailable).toBeTruthy();
    expect(stackLength).toBe(10);
    expect(data).toEqual({
      foo: 'bar',
    });
  });

  it('dispatches action with payload when calling onAction', async () => {
    const { onAction } = useActionStack();
    onAction({ bip: 'bop' });

    expect(dispatch).toHaveBeenCalledWith({
      type: ActionStackActionTypes.ON_ACTION,
      payload: { bip: 'bop' },
    });
  });

  it('dispatches action with payload when calling onUndo', async () => {
    const { onUndo } = useActionStack();
    onUndo(5);

    expect(dispatch).toHaveBeenCalledWith({
      type: ActionStackActionTypes.ON_UNDO,
      payload: 5,
    });
  });

  it('dispatches action with payload when calling onRedo', async () => {
    const { onRedo } = useActionStack();
    onRedo(5);

    expect(dispatch).toHaveBeenCalledWith({
      type: ActionStackActionTypes.ON_REDO,
      payload: 5,
    });
  });
});
