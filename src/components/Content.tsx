import React from 'react';
import useActionStack from '../hooks/useActionStack';

const Content: React.FC = () => {
  const { data: actionStack, isUndoAvailable, isRedoAvailable, stackLength } = useActionStack();

  return (
    <ul>
      <li>data: {JSON.stringify(actionStack)}</li>
      <li>isUndoAvailable: {isUndoAvailable.toString()}</li>
      <li>isRedoAvailable: {isRedoAvailable.toString()}</li>
      <li>stackLength: {stackLength}</li>
    </ul>
  );
};

export default Content;
