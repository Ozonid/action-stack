import React from 'react';
import withActionStack from '../HOC/withActionStack';
import Actions from './Actions';
import Content from './Content';

const Wrapper: React.FC = () => (
  <>
    <Actions />
    <Content />
  </>
);

export default withActionStack(Wrapper, {
  initialValue: { dev: true, qa: true, prod: false },
  onUpdate: (data) => {
    console.log('something happened', data);
  },
});
