import React, { useState } from 'react';
import useActionStack from '../hooks/useActionStack';

const Actions: React.FC = () => {
  const { data: actionStack, onAction, onUndo, onRedo, isUndoAvailable, isRedoAvailable } = useActionStack();

  const [count, setCount] = useState(0);

  const onActionClick = () => {
    onAction({ dev: !actionStack['dev'], bop: count });
    setCount(count + 1);
  };

  return (
    <div>
      <p>
        <button onClick={() => onUndo(1)} disabled={!isUndoAvailable}>
          undo 1
        </button>
        <button onClick={() => onUndo(2)} disabled={!isUndoAvailable}>
          undo 2
        </button>
      </p>
      <p>
        <button onClick={() => onRedo(1)} disabled={!isRedoAvailable}>
          redo 1
        </button>
        <button onClick={() => onRedo(2)} disabled={!isRedoAvailable}>
          redo 2
        </button>
      </p>
      <p>
        <button onClick={onActionClick}>action!</button>
      </p>
    </div>
  );
};

export default Actions;
